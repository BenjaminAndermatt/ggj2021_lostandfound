using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuCtrl : MonoBehaviour
{
	[SerializeField] SpriteRenderer background;
	[SerializeField] Sprite[] backgroundSprites;

	[SerializeField] GameObject mainMenu;
	[SerializeField] GameObject tutorialMenu;
	[SerializeField] Image tutorialScreen;
	[SerializeField] Sprite[] tutorialSprites;

	int tutorialIndex = 0;

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	public void PlayGame() {
		Application.LoadLevel(1);
	}

	public void ShowTutorial() {
		mainMenu.SetActive(false);
		tutorialMenu.SetActive(true);

		background.sprite = backgroundSprites[1];

		tutorialIndex = 0;
		tutorialScreen.sprite = tutorialSprites[0];
	}

	public void CloseTutorial() {
		mainMenu.SetActive(true);
		tutorialMenu.SetActive(false);

		background.sprite = backgroundSprites[0];
	}

	public void ChangeTutorialScreen(int direction) {
		tutorialIndex += direction;

		if (tutorialIndex < 0) tutorialIndex = tutorialSprites.Length-1;
		if (tutorialIndex >= tutorialSprites.Length) tutorialIndex = 0;

		tutorialScreen.sprite = tutorialSprites[tutorialIndex];
	}
}
