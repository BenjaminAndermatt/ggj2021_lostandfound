using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    public enum BirdState {Idle, Chasing, Pickup, Eating }

    public BirdState State = BirdState.Idle;
    public float MovementForce;
    public float MaxMovementVelocity;
    public float JumpForce;
    public float ScareForce;
    public float JumpCooldown;
    public float ScareCooldown;
    public float MinTimeToTurn;
    public float MaxTimeToTurn;
    public float TimeToGiveUp;
    public float TimeToPickUp;
    public float TimeToSwallow;
    public float TimeToPanic;
    public float MaxBreadDistance;
    public Transform Sprite;
    public Transform BreadContainer;
    public float GroundLevel;
    public AudioSource audioSource;

    private Rigidbody2D rigidbody;
    private Vector2 lookDirection = Vector2.right;
    private float nextTurn;
    private float chasingStartTime;
    private float lastJump;
    private float lastScared;
    private float lastTimeOnTheGround;
    private Transform target;
    private Transform bread;
    private bool eating;
    private bool grounded;
    private SpriteController spriteController;
    private SoundManager sound;

    private ThiefController thief;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        spriteController = GetComponent<SpriteController>();
        nextTurn = Time.time + Random.Range(MinTimeToTurn, MaxTimeToTurn);
        thief = ThiefController.Thief;
        sound = SoundManager.sound;
    }

    void FixedUpdate()
    {
        bool breadAvailable = FindBread();

        switch (State)
        {
            case BirdState.Chasing:
                if (breadAvailable)
                {
                    GetBread();
                }
                else
                {
                    GiveUp();
                }
                break;
            case BirdState.Pickup:
                break;
            case BirdState.Eating:
                break;
            default:
                Idle();
                break;
        }

        CheckGrounded();
        Panic();

        SetSprite();
    }

    public bool FindBread()
    {
        if (!eating)
        {
            target = GameManager.game.GetBreadInView(transform.position.x, lookDirection.x > 0f, MaxBreadDistance);
            return target != null;
        }
        return false;
    }

    public void GetBread()
    {
        if (target != null)
        {
            if (rigidbody.velocity.x * lookDirection.x < MaxMovementVelocity)
            {
                rigidbody.AddForce(MovementForce * lookDirection * Time.fixedDeltaTime);
            }
            if (chasingStartTime + TimeToGiveUp < Time.time)
            {
                Break();
                GiveUp();
            }
        }
    }

    public void Break()
    {
        rigidbody.AddForce(rigidbody.velocity * -1f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (target != null)
        {
            if (collision.transform.name == target.name)
            {
                Break();
                StartCoroutine(TakeBread());
            }
        }

        if (collision.tag == "Scream" && lastScared + ScareCooldown < Time.time )
        {
            rigidbody.AddForce((ScareForce * Vector3.up) + (ScareForce * (transform.position - collision.transform.position).normalized));
            lastScared = Time.time;
            if (State == BirdState.Eating)
            {
                DropBread();
            }
            sound.PlaySound(audioSource, sound.Panic, 0.5f);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (State == BirdState.Chasing && collision.transform.tag == "Bird" && Time.time > lastJump + JumpCooldown)
        {
            Jump();
        }
    }


    public void Idle()
    {
        if (Time.time > nextTurn)
        {
            Turn();
        }
        if (target != null)
        {
            chasingStartTime = Time.time;
            State = BirdState.Chasing;
        }
    }

    void Turn()
    {
        Sprite.localScale = new Vector3(Sprite.localScale.x * -1f, Sprite.localScale.y, Sprite.localScale.z);
        lookDirection = new Vector2(lookDirection.x * -1f, 0f);
        nextTurn = Time.time + Random.Range(MinTimeToTurn, MaxTimeToTurn);
    }

    public void GiveUp()
    {
        eating = false;
        State = BirdState.Idle;
        sound.PlaySound(audioSource, sound.Gurr, 0.2f);
    }

    public void Jump()
    {
        rigidbody.AddForce(Vector2.up * JumpForce);
        lastJump = Time.time;
        sound.PlaySound(audioSource, sound.Gurr, 0.2f);
    }

    IEnumerator TakeBread()
    {
        if (target != null)
        {
            bread = target;
            target = null;
            sound.PlaySound(audioSource, sound.PickUp, 0.4f);
            eating = true;
            //rigidbody.isKinematic = true;
            State = BirdState.Pickup;
            bread.parent = BreadContainer;
        }
        else
        {
            GiveUp();
        }
        yield return new WaitForSeconds(TimeToPickUp);
        if (bread != null)
        {
            bread.GetComponent<Rigidbody2D>().simulated = false;
            GameManager.game.Bread.Remove(bread);
            //rigidbody.isKinematic = false;
            bread.position = BreadContainer.position;
            State = BirdState.Eating;
            StartCoroutine(SwallowBread());
            sound.PlaySound(audioSource, sound.Eat, 0.4f);
        }
        else
        {
            GiveUp();
        }
    }

    void DropBread()
    {
        if (bread != null)
        {
            bread.parent = null;
            bread.GetComponent<Rigidbody2D>().simulated = true;
            eating = false;
            GameManager.game.Bread.Add(bread);
            bread = null;
        }
    }

    IEnumerator SwallowBread()
    {
        yield return new WaitForSeconds(TimeToSwallow);
        if (bread != null)
        {
            GameManager.game.RemoveBread(bread);
            GameManager.game.ScoreFeeder++;
            GameManager.game.CheckWin();
        }
        else
        {
            GiveUp();
        }
        eating = false;
        State = BirdState.Idle;
    }

    void CheckGrounded()
    {
        bool currentlyOnGround = transform.position.y <= GroundLevel;

        if (grounded != currentlyOnGround)
        {
            grounded = currentlyOnGround;
            if (!grounded)
            {
                lastTimeOnTheGround = Time.time;
            }
        }
    }

    void Panic()
    {
        //Make a move when stuck
        if (!grounded && lastTimeOnTheGround + TimeToPanic < Time.time)
        {
            rigidbody.AddForce(JumpForce * lookDirection);
            lastTimeOnTheGround = Time.time;
            sound.PlaySound(audioSource, sound.Panic, 0.2f);
        }
    }

    void SetSprite()
    {
        int spriteIndex;

        if (!grounded && !eating)
        {
            spriteIndex = 2;
        }
        else
        {
            switch (State)
            {
                case BirdState.Chasing:
                    spriteIndex = 1;
                    break;
                case BirdState.Eating:
                    spriteIndex = 4;
                    break;
                case BirdState.Pickup:
                    spriteIndex = 3;
                    break;
                default:
                    spriteIndex = 0;
                    break;
            }
        }

        if (spriteIndex != spriteController.currentIndex)
        {
            spriteController.SetSprite(spriteIndex);
        }
    }
}
