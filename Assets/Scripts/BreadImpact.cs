using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreadImpact : MonoBehaviour
{
    public AudioSource audioSource;
    private SoundManager sound;

    private void Start()
    {
        sound = SoundManager.sound;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        sound.PlaySound(audioSource, sound.Bread, 0.5f);
    }
}
