using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeederController : MonoBehaviour
{
    public Vector2 RelativeThrowForce;
    public Vector2 AdditionalThrowForce;
    public GameObject BreadObject;
    public float CoolDown;
    public float DelayToRegisterBread;
    public SpriteRenderer spriteRenderer;
    public Sprite SpriteGrab;
    public Sprite SpriteThrow;
    public float ThrowAnimationTime;
    public AudioSource audioSource;

    private float lastThrow;

    SoundManager sound;


    void Start()
    {
        sound = SoundManager.sound;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && Time.time > lastThrow + CoolDown && GameManager.game.BreadCount > 0)
        {
            Throw();
            lastThrow = Time.time;
            GameManager.game.LowerBreadCount();
        }
    }

    void Throw()
    {
        sound.PlaySound(audioSource, sound.Throw, 0.5f);

        Vector2 throwDirection = (Input.mousePosition - ScreenPosition());
        throwDirection = new Vector3(throwDirection.x / Screen.width, throwDirection.y / Screen.height, 0f);
        GameObject bread = Instantiate(BreadObject, transform.position, Quaternion.identity);
        bread.name = "Bread_" + Time.time;
        Rigidbody2D breadRigidBody = bread.GetComponent<Rigidbody2D>();
        breadRigidBody.AddForce(RelativeThrowForce * throwDirection + AdditionalThrowForce);
        StartCoroutine(AddBreadToList(bread.transform));
        StartCoroutine(ChangeSprite());
    }

    Vector3 ScreenPosition()
    {
        return Camera.main.WorldToScreenPoint(transform.position);
    }

    IEnumerator AddBreadToList(Transform bread)
    {
        yield return new WaitForSeconds(DelayToRegisterBread);
        GameManager.game.Bread.Add(bread);
    }

    IEnumerator ChangeSprite()
    {
        spriteRenderer.sprite = SpriteThrow;
        yield return new WaitForSeconds(ThrowAnimationTime);
        spriteRenderer.sprite = SpriteGrab;
    }
}
