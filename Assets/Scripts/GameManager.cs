using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager game;

    public Text GUI_ScoreFeeder;
    public Text GUI_ScoreThief;
    public Text GUI_BreadCount;
    public Text GUI_WinText;
    public GameObject GUI_WinScreen;

    public int BreadCount;
    public bool GameOver;

    public int ScoreFeeder
    {
        set
        {
            scoreFeeder = value;
            GUI_ScoreFeeder.text = value.ToString();
        }
        get
        {
            return scoreFeeder;
        }
    }
    public int ScoreThief
    {
        set
        {
            scoreThief = value;
            GUI_ScoreThief.text = value.ToString();
        }
        get
        {
            return scoreThief;
        }
    }

    private int scoreFeeder;
    private int scoreThief;

    public GameObject PigeonPrefab;
    public Transform PigeonSpawnPoint;

    public List<Transform> Bread;

    private Transform breadToDestroy;
    private int pigeonCount;

    void Awake()
    {
        game = this;
	}

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel(0);
        }
    }

    void CleanList()
    {
        foreach (Transform b in Bread)
        {
            if (b == null)
            {
                RemoveBread(b);
            }
        }
    }

    private void Start() {
		GUI_BreadCount.text = BreadCount.ToString();
	}
	void FixedUpdate()
    {
        if (breadToDestroy != null)
        {
            Destroy(breadToDestroy.gameObject);
            breadToDestroy = null;
        }

        CleanList();
    }

    public Transform GetBreadInView (float positionX, bool right, float maxDistance)
    {
        if (Bread.Count > 0 && Bread[0] != null)
        {
            Transform bread = Bread[0];
            float shortestDistance = Mathf.Abs(bread.position.x - positionX);

            for (int i = 1; i < Bread.Count; i++)
            {
                if (Bread[i] != null)
                {
                    float currentDistance = Mathf.Abs(Bread[i].position.x - positionX);
                    if ((Bread[i].position.x > positionX) == right && currentDistance < shortestDistance)
                    {
                        bread = Bread[i];
                        shortestDistance = currentDistance;
                    }
                }
            }
            
            if ((bread.position.x > positionX) == right && shortestDistance < maxDistance)
            {
                return bread;
            }
        }
        return null;
    }

    public void RemoveBread (Transform bread)
    {
        Bread.Remove(bread);
        breadToDestroy = bread;
    }

    public void SpawnPigeon()
    {
        GameObject pigeon = Instantiate(PigeonPrefab, PigeonSpawnPoint.position, Quaternion.identity);
        pigeonCount++;
        pigeon.name = PigeonPrefab.name + pigeonCount;
    }

    public void LowerBreadCount()
    {
        BreadCount--;
        GUI_BreadCount.text = BreadCount.ToString();
    }

    public void CheckWin()
    {
        string winText = "";
        if (scoreFeeder-scoreThief > BreadCount)
        {
            winText = "Pigeon feeder wins!";
            Win(winText);
        } else if (scoreThief-scoreFeeder > BreadCount)
        {
            winText = "Seagull wins!";
            Win(winText);
        }
    }

    void Win(string winText)
    {
        GameOver = true;
        GUI_WinScreen.SetActive(true);
        GUI_WinText.text = winText;
    }
}
