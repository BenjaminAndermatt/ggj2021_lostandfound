using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleLoop : MonoBehaviour
{
	[SerializeField] Sprite[] sprites;
	[SerializeField] float framesTillChange;

	SpriteRenderer sr;
	float frameCounter = 0;
	int frame = 0;

    // Start is called before the first frame update
    void Start()
    {
		sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
		frameCounter++;

		if (frameCounter >= framesTillChange) {
			frame++;

			if (frame >= sprites.Length) frame = 0;

			sr.sprite = sprites[frame];

			frameCounter = 0;
		}
    }
}
