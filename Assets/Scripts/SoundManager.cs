using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager sound;

    public AudioClip[] Gurr;
    public AudioClip[] Eat;
    public AudioClip[] Panic;
    public AudioClip[] PickUp;
    public AudioClip[] Throw;
    public AudioClip[] SeagullEat;
    public AudioClip[] Bread;


    void Awake()
    {
        sound = this;
    }

    public void PlaySound(AudioSource source, AudioClip[] clips, float volume)
    {
        source.clip = clips[Random.Range(0, clips.Length)];
        float screenPositionX = Camera.main.WorldToScreenPoint(source.transform.position).x / Screen.width - 0.5f;
        source.panStereo = 0.2f + screenPositionX * 0.8f;
        source.volume = volume;
        source.Play();
    }
}
