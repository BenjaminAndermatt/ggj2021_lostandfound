using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteController : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public SpriteSequence[] SpriteStates;
    private SpriteSequence currentSpirte;
    [HideInInspector] public int currentIndex;

    void Start()
    {
        foreach (SpriteSequence ss in SpriteStates)
        {
            ss.Initialize();
        }

        if (currentSpirte == null)
        {
            SetSprite(0);
        }
    }

    public void SetSprite(int index)
    {
        currentIndex = index;
        currentSpirte = SpriteStates[index];
        spriteRenderer.sprite = currentSpirte.Sprites[0];
    }

    public void Flip(bool flip)
    {
        spriteRenderer.flipX = flip;
    }

    void Update()
    {
        if (currentSpirte.Sprites.Length > 1)
        {
            PlaySequence();
        }

        ZLayerBasedOnHeight();
    }

    void PlaySequence()
    {
        spriteRenderer.sprite = currentSpirte.GetFrameAtTime(Time.time);
    }

    void ZLayerBasedOnHeight()
    {
        spriteRenderer.sortingOrder = 80 - Mathf.FloorToInt(transform.position.y*10f);
    }
}
