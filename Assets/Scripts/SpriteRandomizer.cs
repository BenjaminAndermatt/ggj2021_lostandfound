using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteRandomizer : MonoBehaviour
{
	[SerializeField] Sprite[] sprites;

    // Start is called before the first frame update
    void Start()
    {
		int index = Mathf.FloorToInt(Random.value * sprites.Length);

		GetComponent<SpriteRenderer>().sprite = sprites[index];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
