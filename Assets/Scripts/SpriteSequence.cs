using UnityEngine;

[System.Serializable]
public class SpriteSequence
{
    public Sprite[] Sprites;
    [HideInInspector] public int NumberOfFrames;
    public float FramesPerSecond;

    private int[] frames;
    private float animationTime;

    public void Initialize()
    {
        NumberOfFrames = Sprites.Length;
        animationTime = (NumberOfFrames) / FramesPerSecond;
    }

    public Sprite GetFrameAtTime(float time)
    {
        time %= animationTime;
        int currentFrameIndex = Mathf.FloorToInt(time * FramesPerSecond);
        return Sprites[currentFrameIndex];
    }
}
