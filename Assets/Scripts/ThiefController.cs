using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThiefController : MonoBehaviour
{
    public static ThiefController Thief;
    public GameObject ScreamObject;
    public float MovementForce;
    public float MaxMovementVelocity;
    public float JumpForce;
    public float ScreamCooldown;
    public float EatAttemptCooldown;
    public float MaxBreadDistance;
    public float PickupTime;
    public float EatTime;
    public float ScreamTime;
    public float GroundLevel;
    public AudioSource SeagullAudioSource;
    public float MaxFlightHeight;

    private Rigidbody2D rigidbody;
    private Vector2 lookDirection = Vector2.right;
    private float lastScream;
    private float lastEatAttempt;
    private SpriteController spriteController;
    private int spriteIndex;
    private bool eating;
    private bool screaming;
    private bool grounded;

    //Audio
    public float loudnessThreshold = -2f;
    public bool checkingLoudness = true;

    public float updateStep = 0.1f;
    public int sampleDataLength = 256;

    private float currentUpdateTime = 0f;

    private float clipLoudness;
    private float[] clipSampleData;

    private AudioSource audio;
    private SoundManager sound;


    void Awake()
    {
        Thief = this;
    }

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        CheckMicrophone();
        StartCoroutine(CheckLoudness());
        spriteController = GetComponent<SpriteController>();
        sound = SoundManager.sound;
    }

    private void Update()
    {
        Fly();
        Scream();
    }

    void FixedUpdate()
    {
        Move();

        Eat();

        SetSprite();

        CheckGrounded();
    }

    void Fly()
    {
        if (Input.GetKeyDown(KeyCode.W) && transform.position.y < MaxFlightHeight)
        {
            rigidbody.AddForce(Vector2.up * JumpForce);
        }
    }

    void Eat()
    {
        if (lastEatAttempt + EatAttemptCooldown < Time.time)
        {
            TryEat();
            lastEatAttempt = Time.time;
        }
    }

    void Move()
    {
        float x = Input.GetAxis("Horizontal");
        if (!eating && !screaming)
        {
            if (x > 0.1f)
            {
                lookDirection = Vector2.right;
                spriteController.Flip(true);
                spriteIndex = 1;
            }
            else if (x < -0.1f)
            {
                lookDirection = Vector2.left;
                spriteController.Flip(false);
                spriteIndex = 1;
            }
            else if (!eating && !screaming && grounded)
            {
                spriteIndex = 0;
            }
            if (rigidbody.velocity.x * lookDirection.x < MaxMovementVelocity)
            {
                rigidbody.AddForce(MovementForce * lookDirection * Mathf.Abs(x) * Time.fixedDeltaTime);
            }
        }
    }

    void Scream()
    {
        if (Input.GetKeyDown(KeyCode.Q) || Mathf.Log(clipLoudness) > loudnessThreshold && lastScream + ScreamCooldown < Time.time && !eating)
        {
            screaming = true;
            spriteIndex = 5;
            lastScream = Time.time;
            GameObject scream = Instantiate(ScreamObject, transform.position, Quaternion.identity);
            StartCoroutine(StopScreaming());
        }
    }

    IEnumerator StopScreaming()
    {
        yield return new WaitForSeconds(ScreamTime);
        screaming = false;
    }

    void CheckMicrophone()
    {
        // From the internet
        audio = GetComponent<AudioSource>();
        audio.clip = Microphone.Start(null, true, 1, 22050);
        audio.loop = true;
        while (!(Microphone.GetPosition(null) > 0)) { }
        Debug.Log("start playing... position is " + Microphone.GetPosition(null));
        audio.Play();
        audio.volume = 0f;
        clipSampleData = new float[sampleDataLength];
    }

    IEnumerator CheckLoudness()
    {
        while (checkingLoudness)
        {
            currentUpdateTime += Time.fixedDeltaTime;
            if (currentUpdateTime >= updateStep)
            {
                currentUpdateTime = 0f;
                audio.clip.GetData(clipSampleData, audio.timeSamples);
                clipLoudness = 0f;
                foreach (var sample in clipSampleData)
                {
                    clipLoudness += Mathf.Abs(sample);
                }
                clipLoudness /= sampleDataLength; //clipLoudness is what you are looking for
            }
            yield return null;
        }
    }

    private void TryEat()
    {
        if (!screaming)
        {
            Transform bread = GameManager.game.GetBreadInView(transform.position.x, lookDirection.x > 0f, MaxBreadDistance);

            if (bread != null && transform.position.y - bread.position.y > 0f && transform.position.y - bread.position.y < MaxBreadDistance)
            {
                GameManager.game.RemoveBread(bread);
                GameManager.game.ScoreThief++;
                StartCoroutine(EatBread());
            }
        }
    }

    private IEnumerator EatBread()
    {
        eating = true;
        sound.PlaySound(SeagullAudioSource, sound.SeagullEat, 0.5f);
        spriteIndex = 3;
        yield return new WaitForSeconds(PickupTime);
        spriteIndex = 4;
        yield return new WaitForSeconds(EatTime);
        eating = false;

        GameManager.game.CheckWin();
        GameManager.game.SpawnPigeon();
    }

    private void SetSprite()
    {
        if (spriteIndex != spriteController.currentIndex)
        {
            spriteController.SetSprite(spriteIndex);
        }
    }

    void CheckGrounded()
    {
        grounded = transform.position.y <= GroundLevel;
        if (!grounded && !screaming && !eating)
        {
            spriteIndex = 2;
        }
    }
}
